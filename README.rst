Extremely simple game done on 2013/2014 xmas holidays, in 4 days.

SVGs are rasterized to PNG using a python script (which depends on pygame and rsvg) at Svg/rasterize.py.

Tested for android, linux, html5 but probably works for other targets.

For testing (rasterize images first), run in base directory:
```nme test project.nmml linux```

It's not very documented. Has some dirty approaches for keeping code simply as possible and coding fast.

This is only for testing HAXE and NME using a game as proof of concept.

I definitely enjoyed NME.
