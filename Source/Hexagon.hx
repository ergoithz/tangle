

import nme.display.Bitmap;
import nme.display.BitmapData;
import nme.display.Sprite;
import nme.display.DisplayObject;

import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Sine;
import com.eclecticdesignstudio.motion.actuators.GenericActuator;

import Utils;

class StepPath {
    /*
     * Hexagon path, every hexagon has 6 six of this.
     */
    static var door_codes:String = "123456789abc";
    public var start(default, null):Int;
    public var end(default, null):Int;
    public var active:Bool = false;
    public var disabled:Bool = false;
    private function fix_value(value:Int) {
        if (value < 0)
        {
            value += door_codes.length;
        }
        else if (value >= door_codes.length )
        {
            value -= door_codes.length;
        }
        return value;
    }
    public function new( start:Int, end:Int) {
        set_values(start, end);
    }
    public function set_values(start:Int, end:Int) {
        start = fix_value(start);
        end = fix_value(end);
        if (start < end)
        {
            this.start = start;
            this.end = end;
        }
        else
        {
            this.start = end;
            this.end = start;
        }
    }

    public function get_code():String {
        if(disabled) return "g" + door_codes.charAt(start) + door_codes.charAt(end);
        return (active?"h":"a") + door_codes.charAt(start) + door_codes.charAt(end);
    }
}

class StepPathIter {
    /*
     * Helper iterator used for generating StepPaths extracting integers
     * randomly from array.
     */
    private var array:Array<Int>;
    public function new( array:Array<Int> ) {
        this.array = array.copy();
    }

    public function hasNext():Bool{
        return( array.length > 0 );
    }

    private function pop():Int{
        var r:Int = array[Std.random(array.length)];
        array.remove(r);
        return r;
    }

    public function next():StepPath {
        // Return random StepPath
        return new StepPath(pop(), pop());
    }
}

class Step extends Sprite{
    /*
     * Step (hexagon with paths) class
     *
     * Contains StepPaths, a bitmap, rotation methods and more
     *
     */
    static var doors:Array<Int> = [0,1,2,3,4,5,6,7,8,9,10,11];
    static var rotation_duration:Float = 0.1;

    private var tilt:GenericActuator = null;

    public var field_x:Int;
    public var field_y:Int;

    public var active:Bool;

    public var stepins:Int;

    public var paths(default, set_paths):Array<StepPath>;

    public function new (field_x:Int, field_y:Int) {
        /*
         * Initialize step (with random paths) at given field position
         */
        super ();
        this.field_x = field_x;
        this.field_y = field_y;
        active = (field_x>-1) && (field_y > -1);
        shuffle();
    }

    public function shuffle(){

        var pairs = new StepPathIter(doors);
        var disabled:Bool = (field_x==-1)&&(field_y==-1);
        paths = [];
        for(i in pairs){
            i.disabled = disabled;
            paths.push(i);
        }
        draw_paths();
    }

    private function set_paths(paths:Array<StepPath>):Array<StepPath>{
        var disabled:Bool = (field_x==-1)&&(field_y==-1);
        this.paths = paths;
        for(i in paths) i.disabled = disabled;
        draw_paths();
        return paths;
    }

    public static function switch_paths(step1:Step, step2:Step){
        // Switch paths between two steps, used for alternative step change
        var stepath:Array<StepPath> = step1.paths;
        step1.paths = step2.paths;
        step2.paths = stepath;
        }

    private function rotate_paths(more:Int) {
        for(i in 0...paths.length)
            paths[i].set_values(paths[i].start+more, paths[i].end+more);
        draw_paths();
    }

    public function rotate_left(){
        /*
         * Rotate animations are done in two steps, due irregular hexagon shape
         */
        Actuate
          .tween (this, rotation_duration, {rotation:-30} )
          .ease (Sine.easeIn)
          .onComplete(function(){
            rotate_paths(-2);
            rotation = 30;
            Actuate
                .tween (this, rotation_duration, {rotation:0})
                .ease (Sine.easeOut);
            });
        }

    public function rotate_right(){
        /*
         * Rotate animations are done in two steps, due irregular hexagon shape
         */
        Actuate
          .tween (this, rotation_duration, {rotation:30} )
          .ease (Sine.easeIn)
          .onComplete(function(){
            rotate_paths(2);
            rotation = -30;
            Actuate
                .tween (this, rotation_duration, {rotation:0})
                .ease (Sine.easeOut);
            });
        draw_paths();
    }

    public function get_stepout(stepin:Int){
        // Get stepout for given stepin
        for(i in paths){
            if(i.start == stepin) return i.end;
            else if(i.end == stepin) return i.start;
        }
        return 0;
    }

    public function enter_stepin(stepin:Int){
        // Activate path for given stepin
        var changed:Bool = false;
        for(i in paths){
            if((i.start == stepin)||(i.end == stepin)){
                if(!i.active){
                    i.active = true;
                    changed = true;
                    }
                stepins += 1;
                break;
            }
        }
        if(changed) draw_paths();
    }

    private function draw_paths(){
        // Redraw step's paths (highlighting active ones)
        if((tilt!=null)&&(!active)) Actuate.stop(tilt);
        var bg:Bitmap;
        while(numChildren > 0) removeChildAt(0);
        if(paths.length > 0)
        {
            if(active){
                bg = Utils.getBitmap("abg");
                addChild(bg);
                Actuate.tween(bg, 1, {alpha:0.75}).ease(Sine.easeInOut).reflect().repeat();
                }

            for(i in paths) addChild(Utils.getBitmap(i.get_code()));
            // Center everything
            var bitmap:DisplayObject = getChildAt(0);
            var bx:Float = -bitmap.width/2;
            var by:Float = -bitmap.height/2;
            for(i in 0...numChildren)
            {
                bitmap = getChildAt(i);
                bitmap.x = bx;
                bitmap.y = by;
            }
        }
    }
}
