

import nme.display.PixelSnapping;
import nme.display.Bitmap;
import nme.Assets;

import nme.events.Event;

class Utils {
    public static function getBitmap(name:String):Bitmap
    {
        return new Bitmap (Assets.getBitmapData ("assets/"+name+".png"), PixelSnapping.AUTO, true);
    }

    public static function stopPropagation(event:Event)
    {
        // stopPropagation bug workarounds
        #if cpp
        Reflect.setField(event, "nmeIsCancelled", true);
        #elseif html5
        event.stopImmediatePropagation();
        #else
        event.stopPropagation();
        #end
    }

    public static  function minmax(min:Float, v:Float, max:Float):Float
    {
        if(v < min) return min;
        if(v < max) return v;
        return max;
    }

}
