
import Utils;
import nme.display.Bitmap;
import nme.display.Sprite;

class Score extends Sprite{
    public var score(default, set_score):Int;
    public var digits:Int;
    public function new(){
        super();
        score = 0;
        }

    private function set_score(v:Int):Int{
        var num:Int = v;
        var next_num:Int;
        var offset:Int = 0;
        var bitmap:Bitmap;
        var old_width:Float = width;
        while(numChildren > 0) removeChildAt(0);
        digits = 0;
        while(num > 0){
            next_num = Std.int(num/10);
            bitmap = Utils.getBitmap("score_" + Std.string(num-next_num*10));
            bitmap.width = 1;
            bitmap.height = 2;
            bitmap.x = offset;
            addChild(bitmap);
            offset -= 1;
            num = next_num;
            digits += 1;
        }
        while (digits<3){
            bitmap = Utils.getBitmap("score_0");
            bitmap.width = 1;
            bitmap.height = 2;
            bitmap.x = offset;
            addChild(bitmap);
            offset -= 1;
            digits += 1;
        }
        for(i in 0...numChildren){
            getChildAt(i).x = 1*(digits-i-1);
            }
        x += old_width-width;
        return v;
    }
}
