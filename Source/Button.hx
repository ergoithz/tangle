

import nme.display.Sprite;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.MouseEvent;
import nme.events.TouchEvent;

import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Sine;
import com.eclecticdesignstudio.motion.actuators.GenericActuator;

import Utils;

class Button extends Sprite{
    /*
     * Sprite with click/touch callback
     *
     * Options:
     *   code: bitmap code as Strimng
     *   cback: callback function Void -> Void
     *   sensitive: True if button is clickable, as Bool
     *   clicked: True if button is currently clicked, as Bool
     */
    public var code:String;
    public var cback:Void -> Void;
    public var sensitive(default, set_sensitive):Bool = true;
    public var clicked:Bool = false;
    private var bitmap:Bitmap;
    private var actuator:GenericActuator = null;
    public function new(code:String, cback:Void -> Void){
        super ();
        this.bitmap = Utils.getBitmap(code);
        this.cback = cback;
        addChild(bitmap);
        addEventListener (MouseEvent.MOUSE_DOWN, mouseDown);
        addEventListener (MouseEvent.MOUSE_UP, mouseUp);
        addEventListener (MouseEvent.MOUSE_OUT, mouseOut);
        addEventListener (MouseEvent.CLICK, mouseClick);
        addEventListener (TouchEvent.TOUCH_OUT, mouseOut);
    }

    private function mouseOut(event:Event){
        if(clicked)
        {
            alpha = 1;
            clicked = false;
        }
    }

    private function mouseClick(event:Event){
        if(!sensitive) Utils.stopPropagation(event);
    }

    private function mouseDown(event:Event){
        push();
        if(sensitive) Utils.stopPropagation(event);
    }

    private function mouseUp(event:Event){
        release();
        if(sensitive) Utils.stopPropagation(event);
    }

    private function stop_actuator()
    {
        if(actuator!=null)
        {
            actuator.stop({alpha:1}, false, false);
            actuator = null;
        }
    }

    public function push(){
        if(sensitive)
        {
            stop_actuator();
            alpha = 0.1;
            clicked = true;
        }

    }

    public function release(){
        if(clicked)
        {
            clicked = false;
            actuator = cast(Actuate.tween (this, 0.5, {alpha:1} ).ease (Sine.easeOut), GenericActuator);
            cback();
        }

    }

    private function set_sensitive(v:Bool):Bool {
        stop_actuator();
        if(v) alpha = 1;
        else alpha = 0.25;
        sensitive = v;
        return v;
    }

}
