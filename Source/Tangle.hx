package;


import nme.display.DisplayObject;
import nme.display.Bitmap;
import nme.display.Sprite;
import nme.display.StageAlign;
import nme.display.StageScaleMode;
import nme.geom.Rectangle;

import nme.Assets;
import nme.Lib;
import nme.events.Event;
import nme.events.MouseEvent;

import com.eclecticdesignstudio.motion.Actuate;
import com.eclecticdesignstudio.motion.easing.Sine;

import Hexagon;
import Utils;
import Button;
import Score;

/**
 * @author Felipe A. Hernandez
 *
 * Compilation (in nmml path):
 *   nme test project.nmml linux [-64]
 *
 * Glossary
 *   step: every hexagon on field put by player, Step (Sprite)
 *   path: one of every possible way included in step, StepPath
 *   stepin: where a path is connected to previous step, Integer
 *   stepout: where active path (whose stepin is connected) connects with next step, Integer
 *   field: where steps could be located, if a stepout points outside field game is over, Tangle (Sprite)
 */
class Tangle extends Sprite {

    private var background:Bitmap;
    private var field:Bitmap;
    private var field_obscured:Rectangle = null;
    private var button_clockwise:Button;
    private var button_clockunwise:Button;
    private var button_ok:Button;
    private var button_switch:Button;
    private var steps:Array<Step>;
    private var gameover:Bitmap;
    private var altstep:Step = null;

    private var score:Score;
    private var playing(default, set_playing):Bool;

    public var gridx:Float;
    public var gridy:Float;
    public var gridh:Float;
    public var gridoffx:Float;
    public var gridoffy:Float;

    /*
    // Ugly background experiment vars
    public var starfieldwidth:Float = 0;
    public var starfieldheight:Float = 0;
    public var numstars:Int = 0;
    public var starsize:Float = 0;
    */

    public function new () {
        super ();
        steps = [];
        addEventListener (Event.ADDED_TO_STAGE, this_onAddedToStage);
    }

    private function set_playing(v:Bool):Bool{
        /*
         * playing setter
         * Activate or deactivate button response and more
         * Used for showing game over.
         */
        button_clockwise.sensitive = v;
        button_clockunwise.sensitive = v;
        button_ok.sensitive = v;
        button_switch.sensitive = v;
        playing = v;
        altstep.alpha = v?1:0.25;
        return v;
    }

    public function field_resize () {
        var max_width = stage.stageWidth*0.9;
        var max_height = stage.stageHeight*0.99;
        var height = Math.min(max_width*11/14, max_height);
        var width = Math.min(max_width, max_height*14/11);

        background.width = stage.stageWidth;
        background.height = stage.stageHeight;

        field.width = width;
        field.height = height*12/11;
        field.x = (stage.stageWidth-width)/2;
        field.y = (stage.stageHeight-height)/2;
        gridx = width/7;
        gridy = height*3/22;
        gridh = gridy*1.34;
        gridoffx = gridx/2;
        gridoffy = gridh/2;

        gameover.width = field.width;
        gameover.height = field.height;
        gameover.x = field.x;
        gameover.y = field.y;

        if(field_obscured==null)
            field_obscured = new Rectangle(0,0,1,1);
        field_obscured.width = field.width;// * 0.75;
        field_obscured.height = field.height;// * 0.75;
        field_obscured.x = field.x + (field.width-field_obscured.width)/2;
        field_obscured.y = field.y + (field.width-field_obscured.height)/2;
    }

    private function check_field(x:Int, y:Int):Bool {
        /*
         * Test if position is in field
         */
        if((y < 0) || (y > 6)) return false;
        if((x < 0) || (x > (6 - Math.abs(y - 3)))) return false;
        if((x == 3) && (y == 3)) return false;
        return true;
    }

    private function step_resize(step:Step, recent:Bool) {
        /*
         * Field's step moving and resize for fluid layout
         */
        var field_x_offset:Float = gridx*Math.abs(step.field_y - 3)/2;

        if(step.active)
        {
            if(recent)
            {
                step.width = gridx*1.25;
                step.height = gridh*1.25;
                Actuate.tween(step, 0.25, {width:gridx, height:gridh}).ease(Sine.easeIn);
            }
            else
            {
                step.width = gridx;
                step.height = gridh;
            }
        }
        else
        {
            step.width = gridx;
            step.height = gridh;
        }
        step.y = gridoffx + field.y + gridy * step.field_y;
        step.x = gridoffy + field.x + gridx * step.field_x + field_x_offset;
        }

    private function steps_resize () {
        /*
         * Use step_resize on every step for fluid layout
         */
        for(step in steps){
            step_resize(step, false);
        }
    }

    private function hud_resize () {
        /*
         * Resize and move buttons for fluid layout
         */
        var hud_margin:Float = Math.min(stage.stageHeight, stage.stageWidth)/100;
        var buttonsize:Float = field.width / 5;
        var buttonleft:Float = Math.max(hud_margin, field.x-buttonsize);
        var buttonright:Float = Math.min(stage.stageWidth-buttonsize-hud_margin, field.x+field.width);
        var buttontop:Float = hud_margin;
        var buttonbottom:Float = stage.stageHeight-buttonsize-hud_margin;
        button_clockwise.width = buttonsize;
        button_clockwise.height = buttonsize;
        button_clockunwise.width = buttonsize;
        button_clockunwise.height = buttonsize;
        button_ok.width = buttonsize;
        button_ok.height = buttonsize;
        button_switch.width = buttonsize;
        button_switch.height = buttonsize;

        button_clockunwise.x = buttonleft;
        button_clockunwise.y = buttonbottom;

        button_clockwise.x = buttonright;
        button_clockwise.y = buttonbottom;

        button_ok.x = buttonleft;
        button_ok.y = buttontop;

        button_switch.x = buttonright;
        button_switch.y = buttontop;

        altstep.width = buttonsize/2;
        altstep.height = buttonsize/2;
        altstep.x = buttonright+buttonsize/2;
        altstep.y = buttontop+buttonsize/2;

        score.width = gridx*0.75;
        score.height = score.width*2/score.digits;
        score.x = (stage.stageWidth-score.width)/2;
        score.y = (stage.stageHeight-score.height)/2;
        }

    private function new_step () {
        /*
         * Adds new step on field and calculates new score
         */
        var step:Step;

        var stepout:Int;
        var new_x:Int = 3;
        var new_y:Int = 2;
        var new_stepin:Int = 7;
        var score:Int = 0;

        for(step in steps) step.stepins = 0; // Stepins counter reset

        var free:Bool = (steps.length == 0);
        while(!free){
            free = true;
            for(step in steps){
                if ((step.field_x == new_x) && (step.field_y == new_y)){
                    // Fieldpos occupied, pass through it
                    free = false;
                    step.active = false;
                    step.enter_stepin(new_stepin);
                    score += step.stepins;
                    // Calc new position based on current stepout and pos
                    stepout = step.get_stepout(new_stepin);
                    new_x = step.field_x;
                    new_y = step.field_y;
                    switch(stepout){
                        // Switches has no break in Haxe
                        case 0:
                            new_y -= 1;
                        case 1:
                            new_y -= 1;
                        case 2:
                            new_x += 1;
                        case 3:
                            new_x += 1;
                        case 4:
                            new_x += 1;
                            new_y += 1;
                        case 5:
                            new_x += 1;
                            new_y += 1;
                        case 6:
                            new_y += 1;
                        case 7:
                            new_y += 1;
                        case 8:
                            new_x -= 1;
                        case 9:
                            new_x -= 1;
                        case 10:
                            new_x -= 1;
                            new_y -= 1;
                        case 11:
                            new_x -= 1;
                            new_y -= 1;
                    }
                    // Position correction (due field's strange shape)
                    if((new_y > 3) && (new_y > step.field_y)) new_x -= 1;
                    else if ((new_y >= 3) && (new_y < step.field_y)) new_x += 1;
                    // Next stepin calculation
                    new_stepin = stepout + ((stepout % 2 == 0)?7:5);
                    if (new_stepin > 11) new_stepin -= 12;
                    break;
                }
            }
        }
        this.score.score = score;
        // Test if new step is inside field
        if (check_field(new_x, new_y))
        {
            step = new Step(new_x, new_y);
            steps.push(step);
            addChildAt(step, numChildren-5);
            step_resize(step, true);
        }
        else
        {
            playing = false;
            gameover.alpha = 0;
            addChild(gameover);
            Actuate.tween (gameover, 0.25, {alpha:1} ).ease (Sine.easeInOut);
        }
    }

    private function rotate_left(){
        // Rotate last step to left
        steps[steps.length-1].rotate_left();
        }

    private function rotate_right(){
        // Rotate last step to right
        steps[steps.length-1].rotate_right();
        }

    private function switch_step(){
        // Change last step paths with alternative step
        Step.switch_paths(steps[steps.length-1], altstep);
        }

    public static function main () {
        /*
         * Entry point, all draw stuff must be deferred until added to stage,
         * so all processing is on this_onAddedToStage for prevent segfaults
         * on static targets.
         */
        /*
        // Non-working HTML5 workaround for full canvas
        #if js
            js.Lib.document.getElementById("haxe:jeash").style.width = "100%";
            js.Lib.document.getElementById("haxe:jeash").style.height = "100%";
            //Lib.current.stage.width = js.Lib.window.innerWidth;
            //Lib.current.stage.height = js.Lib.window.innerHeight;
            var event = new Event(Event.RESIZE);
            event.target = Lib.current.stage;
            Lib.current.stage.dispatchEvent(event);
        #end
        */

        Lib.current.addChild (new Tangle ());
    }

    // Handlers

    private function stage_onResize (event:Event) {
        // Resize handle for fluid layout
        field_resize();
        steps_resize();
        hud_resize();
        //resize_stars();
    }

    private function stage_onClick (event:Event) {
        /*
         * Click callback for game over's game restart
         */
        if(!playing)
        {
            start_game();
        }
    }

    private function stage_onMouseDown (event:MouseEvent) {
        /*
         * Alternative step paths are drawed by this class over button.
         * Due this dirty trick, this event must be handled by this class
         * instead button itself.
         */
        if(altstep.hitTestPoint(event.stageX, event.stageY))
        {
            button_switch.push();
            Utils.stopPropagation(event);
        }
    }

    private function stage_onMouseUp (event:MouseEvent) {
        /*
         * Alternative step paths are drawed by this class over button.
         * Due this dirty trick, this event must be handled by this class
         * instead button itself.
         */
        if(altstep.hitTestPoint(event.stageX, event.stageY))
        {
            button_switch.release();
            Utils.stopPropagation(event);
        }
    }

    private function start_game(){
        // Remove all steps
        while(steps.length > 0) removeChild(steps.pop());
        if(contains(gameover))
            // Hide gameover bitmap
            Actuate.tween (gameover, 0.1, {alpha:0} ).ease (Sine.easeInOut)
                .onComplete(function(){
                    removeChild(gameover);
                });
        playing = true;
        altstep.shuffle();
        new_step(); // put first step
    }

    /*
    // Ugly and dirty background experiment
    private function fill_stars(x:Float, y:Float, w:Float, h:Float)
    {
        var star:Bitmap;
        var numstars:Int = Std.int(w*h / (Math.pow(gridx, 2)*2));
        if(numstars>0)
            for(i in 0...6)
                for(j in 0...(Std.random(numstars)+numstars))
                {
                    star = Utils.getBitmap("star"+Std.string(i));
                    star.width = star.height = starsize + Std.random(Std.int(starsize))/10;
                    star.x = x+Std.random(Std.int(w));
                    star.y = y+Std.random(Std.int(h));
                    star.alpha = 0.75 + Std.random(25)/100;
                    addChildAt(star, 1);
                    this.numstars += 1;
                }
    }

    private function resize_stars(){
        var mult:Float = starsize;
        var star:DisplayObject;

        starsize = gridx/3;
        mult = starsize/mult;
        for(i in 0...numstars){
            star = getChildAt(i);
            star.width = star.height = star.width*mult;
            }
        if(starfieldwidth < stage.stageWidth)
            fill_stars(starfieldwidth, 0, stage.stageWidth-starfieldwidth, stage.stageHeight);
        if((0 < starfieldwidth)&&(starfieldheight < stage.stageHeight))
            fill_stars(0, starfieldheight, starfieldwidth, stage.stageHeight-starfieldheight);
        starfieldwidth = Math.max(stage.stageWidth, starfieldwidth);
        starfieldheight = Math.max(stage.stageHeight, starfieldheight);
    }

    private function light_stars(){
        var oldsize:Float;
        var star:Bitmap;
        var maxsize:Float = starsize*1.5;
        var minsize:Float = starsize*0.8;
        for(i in 0...numstars){
            star = cast(getChildAt(i+1), Bitmap);
            if(!field_obscured.contains(star.x, star.y))
            {
                oldsize = star.width;
                star.width = star.height = Utils.minmax(
                    minsize,
                    oldsize + Std.random(Std.int(oldsize))*(Std.random(3)-1)/10,
                    maxsize
                    );
                star.x += (oldsize-star.width)/2;
                star.y += (oldsize-star.height)/2;
            }
            else
            {
                trace([star.x, star.y]);
            }
        }
    }
    */

    private function this_onAddedToStage (event:Event) {
        /*
         * First draw stuff (sort of entry point) once added to Stage
         */
        stage.align = StageAlign.TOP_LEFT;
        stage.scaleMode = StageScaleMode.NO_SCALE;

        background = Utils.getBitmap("background");
        field = Utils.getBitmap("field");
        gameover = Utils.getBitmap("gameover");
        field_resize();

        button_clockwise = new Button("clockwise", rotate_right);
        button_clockunwise = new Button("clockunwise", rotate_left);
        button_ok = new Button("ok", new_step);
        button_switch = new Button("switch", switch_step);

        score = new Score();
        altstep = new Step(-1, -1);

        hud_resize();

        addChild (background);
        addChild (field);
        addChild (button_clockwise);
        addChild (button_clockunwise);
        addChild (button_ok);
        addChild (button_switch);
        addChild (altstep);
        addChild (score);

        /*
        resize_stars();
        Actuate.timer(0.5).onRepeat(light_stars).repeat();
        */

        start_game();

        stage.addEventListener (Event.RESIZE, stage_onResize); // Fluid layout
        stage.addEventListener (MouseEvent.CLICK, stage_onClick); // Game over click to play

        // Alternative step button paths (is simpler to draw paths over it and catch events)
        stage.addEventListener (MouseEvent.MOUSE_DOWN, stage_onMouseDown);
        stage.addEventListener (MouseEvent.MOUSE_UP, stage_onMouseUp);

    }


}
