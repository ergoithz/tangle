#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import pygame.image
import itertools

from svgsurface import XMLTag, SVGSurface

def join_pairs(a):
    a = iter(a)
    for i in a:
        yield i+a.next()

def save_svg(svg, size, name, folder="Assets"):
    pygame.image.save(SVGSurface(svg, size), "../%s/%s" % (folder, name))

hexsize = (150, 150)

if __name__ == "__main__":
    f = open("app.svg","rb")
    svg = f.read()
    f.close()
    for size in (36, 48, 72, 96, 114, 512):
        save_svg(svg, (size, size), "%d.png" % size, "Icons")

    fieldsize = tuple(i*7 for i in hexsize)
    f = open("field.svg","rb")
    svg = f.read()
    f.close()
    for g in XMLTag.byTagName(svg, "g"):
        if g.attributes.get("inkscape:label", None) in ("field", "gameover"):
            g.style["display"] = "auto"
            save_svg(g.apply(), fieldsize, "%s.png" % g.attributes["inkscape:label"])

    backgroundsize = (fieldsize[0], fieldsize[1]/12)
    f = open("background.svg","rb")
    svg = f.read()
    f.close()
    save_svg(svg, backgroundsize, "background.png")

    '''
    starsize = tuple(i/2 for i in hexsize)
    f = open("star.svg", "rb")
    svg = f.read()
    f.close()
    for n, color in enumerate(("0000ff", "ff6600", "ff4300", "65ff00", "6600ff", "e500eb")):
        save_svg(svg.replace("0000ff", color), fieldsize, "star%d.png" % n)
    '''

    buttonsize = tuple(i/5 for i in fieldsize)
    for button in ("clockwise", "clockunwise", "ok", "switch"):
        f = open("%s.svg" % button,"rb")
        svg = f.read()
        f.close()
        save_svg(svg, buttonsize, "%s.png" % button)

    scoresize = (max(buttonsize)/2, max(buttonsize))
    f = open("score.svg","rb")
    svg = f.read()
    f.close()
    for g in XMLTag.byTagName(svg, "g"):
        if "inkscape:label" in g.attributes and g.attributes["inkscape:label"].isdigit():
            g.style["display"] = "auto"
            save_svg(g.apply(), scoresize, "score_%s.png" % g.attributes["inkscape:label"])

    f = open("hexaedro.svg","rb")
    svg = f.read()
    f.close()

    wiring = [
        "".join(p) for p in itertools.permutations("123456789abc", 2)
        if all(p[i-1] < p[i] for i in xrange(1, 2))
        ]

    layers = ["abg"]
    layers.extend(wiring)
    for g in XMLTag.byTagName(svg, "g"):
        if "inkscape:label" in g.attributes:
            name = g.attributes["inkscape:label"]
            if g.attributes["inkscape:label"] in layers:
                g.style["display"] = "auto"
                newsvg = g.apply()

                if name in wiring:
                    h = XMLTag.byTagName(newsvg, "path", start=g.start).next()
                    h.style["fill"] = "#666666"
                    h.style["fill-opacity"] = "0"
                    h.style["stroke"] = "#666666"
                    h.style["stroke-opacity"] = "1"
                    save_svg(h.apply(), hexsize, "g%s.png" % name)
                    h = XMLTag.byTagName(newsvg, "path", start=g.start).next()
                    h.style["fill"] = "#ffbf1a"
                    h.style["fill-opacity"] = "0"
                    h.style["stroke"] = "#ffbf1a"
                    h.style["stroke-opacity"] = "0.3"
                    save_svg(h.apply(), hexsize, "a%s.png" % name)
                    h = XMLTag.byTagName(newsvg, "path", start=g.start).next()
                    h.style["fill"] = "#ff6600"
                    h.style["stroke"] = "#ff6600"
                    h.style["stroke-opacity"] = "1"
                    save_svg(h.apply(), hexsize, "h%s.png" % name)
                else:
                    save_svg(newsvg, hexsize, "%s.png" % name)




